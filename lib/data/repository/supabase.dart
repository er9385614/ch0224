import 'dart:math';

import 'package:ch0224/data/models/model_auth.dart';
import 'package:ch0224/data/models/model_profile.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(
    ModelAuth modelAuth
    ) async {
  return await supabase.auth.signUp(
    email: modelAuth.email,
    password: modelAuth.password);
}

Future<void> dataSignUp(
    String userId,
    String phone,
    String fullName
    ) async {
  return supabase.from('profiles').insert({
    'id_user': userId,
    'fullname': fullName,
    'phone': phone
  });
}


Future<AuthResponse> signIn(
    ModelAuth modelAuth
    ) async {
  return supabase.auth.signInWithPassword(
      email: modelAuth.email,
      password: modelAuth.password);
}

Future<void> signOut() async {
  return supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(
    String email,
    String code
    ) {
  return supabase.auth.verifyOTP(
      email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) {
  return supabase.auth.updateUser(UserAttributes(password: password));
}

Future<ModelProfile> getIdUser(String id) async {
  var response = await supabase
      .from('users')
      .select()
      .eq('id_user', supabase.auth.currentUser!.id);
  return ModelProfile.fromJson(response.single);
}

Future<List<ModelProfile>> getUserData() async {
  var response = await supabase.from('profiles').select();
  return response.map((e) => ModelProfile.fromJson(e)).toList();
}

String? getUserId(){
  return supabase.auth.currentUser!.id;
}

Future<void> saveOrders(
    String addressOrder,
    String countryOrder,
    String phoneOrder,
    String othersOrder,
    String items,
    String weight,
    String worth,
    int charges,
    int instant,
    String addressDes,
    String countryDes,
    String phoneDes,
    String othersDes,
    ) async {
  var currentId = getUserId();
  await supabase.from('orders').insert(
      {
        'id_user': currentId,
        'address': addressOrder,
        'country': countryOrder,
        'phone': phoneOrder,
        'others': othersOrder,
        'package_items': items,
        'weight_items': weight,
        'worth_items': worth,
        'delivery_charges': charges,
        'instant_delivery': instant,
        'tax_and_service_charges': (charges+instant)*0.05,
        'sum_price': charges + instant + (charges+instant)*0.05
      }
  );
  await supabase.from('destinations_details').insert(
      {
        'id_user': currentId,
        'address': addressDes,
        'country': countryDes,
        'phone': phoneDes,
        'others': othersDes,
      }
  );
  await supabase.from('transaction').insert(
      {
        'id_user': currentId,
        'sum': charges + instant + (charges+instant)*0.05,
        'title': 'Delivery fee'
      }
  );
}