import 'dart:typed_data';

class ModelProfile{
  final String idUser;
  final String fullName;
  final String phone;
  final Uint8List? avatar;
  final int balance;
  final bool isRider;
  final String createdAt;

  ModelProfile(
      {
        required this.idUser,
        required this.fullName,
        required this.phone,
        required this.avatar,
        required this.balance,
        required this.isRider,
        required this.createdAt
      }
    );
  static ModelProfile fromJson(Map<String, dynamic> json){
    return ModelProfile(
        idUser: json["id_user"],
        fullName: json["fullname"],
        phone: json["phone"],
        avatar: json['avatar'],
        balance: json['balance'],
        isRider: json['rider'],
        createdAt: json['created_at']);
  }
}