import 'dart:ui';

abstract class ColorsApp{

  abstract final Color text;
  abstract final Color subText;
  abstract final Color hint;
  abstract final Color iconTint;
  abstract final Color background;
  abstract final Color accent;
  abstract final Color primary;
  abstract final Color error;

}

class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => const Color.fromARGB(255, 5, 96, 250);

  @override
  // TODO: implement background
  Color get background => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement error
  Color get error => const Color.fromARGB(255, 237, 58, 58);

  @override
  // TODO: implement hint
  Color get hint => const Color.fromARGB(255, 207, 207, 207);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color.fromARGB(255, 20, 20, 20);

  @override
  // TODO: implement primary
  Color get primary => const Color.fromARGB(255, 235, 188, 46);

  @override
  // TODO: implement subText
  Color get subText => const Color.fromARGB(255, 167, 167, 167);

  @override
  // TODO: implement text
  Color get text => const Color.fromARGB(255, 58, 58, 58);

}

class DarkColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => const Color.fromARGB(255, 5, 96, 250);

  @override
  // TODO: implement background
  Color get background => const Color.fromARGB(255, 0, 13, 29);

  @override
  // TODO: implement error
  Color get error => const Color.fromARGB(255, 237, 58, 58);

  @override
  // TODO: implement hint
  Color get hint => const Color.fromARGB(255, 167, 167, 167);

  @override
  // TODO: implement iconTint
  Color get iconTint => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement primary
  Color get primary => const Color.fromARGB(255, 235, 188, 46);

  @override
  // TODO: implement subText
  Color get subText => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement text
  Color get text => const Color.fromARGB(255, 255, 255, 255);

}