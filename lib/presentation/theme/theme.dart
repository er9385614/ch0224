import 'package:ch0224/presentation/theme/colors.dart';
import 'package:flutter/material.dart';

var light = LightColorsApp();

var lightTheme = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: TextStyle(
        color: light.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: light.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: light.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: light.subText)
    ),
  ),
  textTheme: TextTheme(
    titleLarge: TextStyle(
        color: light.text,
        fontSize: 24,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 5/4
    ),
    titleMedium: TextStyle(
        color: light.subText,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 8/7
    ),
    titleSmall: TextStyle(
        color: light.hint,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 8/7
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        textStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            color: light.accent,
            height: 1),
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
        side: BorderSide(color: light.accent),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4)
        ),
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: light.text,
              height: 1),
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
          backgroundColor: light.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: light.accent,
          disabledForegroundColor: light.background
      )
  ),
);

var dark = LightColorsApp();

var darkTheme = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: TextStyle(
        color: dark.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: dark.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: dark.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: light.subText)
    ),
  ),
  textTheme: TextTheme(
    titleLarge: TextStyle(
        color: dark.text,
        fontSize: 24,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 5/4
    ),
    titleMedium: TextStyle(
        color: dark.subText,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 8/7
    ),
    titleSmall: TextStyle(
        color: dark.hint,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w500,
        height: 8/7
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        textStyle: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
            color: dark.accent,
            height: 1),
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
        side: BorderSide(color: dark.accent),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4)
        ),
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              color: dark.text,
              height: 1),
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
          backgroundColor: dark.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: dark.accent,
          disabledForegroundColor: dark.background
      )
  ),
);