import 'package:flutter/material.dart';


class CustomTextFieldPage extends StatefulWidget {
  final String hint;
  final TextEditingController controller;
  const CustomTextFieldPage({super.key, required this.hint, required this.controller});

  @override
  State<CustomTextFieldPage> createState() => _CustomTextFieldPageState();
}
class _CustomTextFieldPageState extends State<CustomTextFieldPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 5),
        Container(
          height: 32,
          width: double.infinity,
          decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(38, 0, 0, 0),
                  blurRadius: 2,
                  offset: Offset(0, 2),
                )
              ]
          ),
          child: TextField(
            controller: widget.controller,
            style: const TextStyle(
                color: Color.fromARGB(255, 58, 58, 58),
                fontSize: 12,
                fontWeight: FontWeight.w400
            ),
            decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent)
                ),
                contentPadding: EdgeInsets.only(left: 8, bottom: 15),
                hintText: widget.hint,
                hintStyle: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color.fromARGB(255, 207, 207, 207)
                )
            ),
          ),
        )
      ],
    );
  }
}