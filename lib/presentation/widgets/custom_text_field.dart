import 'package:ch0224/presentation/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';


class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomTextField(
      {super.key,
      required this.label,
      required this.hint,
      required this.controller,
      this.enableObscure = false,
      this.onChange,
      this.isValid = true});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    var theme = Theme.of(context).inputDecorationTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 24,
        ),
        Text(
          widget.label,
          style: Theme.of(context)
              .textTheme
              .titleMedium
              ?.copyWith(color: colors.subText),
        ),
        const SizedBox(height: 8),
        TextField(
          obscureText: (widget.enableObscure) ? isObscure : false,
          style: Theme.of(context)
              .textTheme
              .titleMedium
              ?.copyWith(color: colors.text),
          obscuringCharacter: "*",
          controller: widget.controller,
          onChanged: widget.onChange,
          decoration: InputDecoration(
              enabledBorder:
                  (widget.isValid) ? theme.enabledBorder : theme.errorBorder,
              focusedBorder:
                  (widget.isValid) ? theme.focusedBorder : theme.errorBorder,
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              hintText: widget.hint,
              hintStyle: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(color: colors.hint),
              suffixIcon: (widget.enableObscure)
                  ? GestureDetector(
                      onTap: () {
                        setState(() {
                          isObscure = !isObscure;
                        });
                      },
                      child: SvgPicture.asset('assets/eye-slash.svg',
                          fit: BoxFit.scaleDown,
                          colorFilter: ColorFilter.mode(
                              colors.iconTint, BlendMode.dst)),
                    )
                  : null),
        ),
      ],
    );
  }
}
