import 'package:flutter/material.dart';

Future<void> showError(BuildContext context, String error) async {
  await showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ошибка"),
    content: Text(error),
    actions: [
      TextButton(onPressed: (){Navigator.pop(context);}, child: const Text("OK"))
    ],
  ));
}

void showLoading(BuildContext context){
  showDialog(context: context, builder: (_) => PopScope(
      canPop: false,
      child: Dialog(
        backgroundColor: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      )
  )
  );
}
