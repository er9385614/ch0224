import 'package:ch0224/domain/log_in_presenter.dart';
import 'package:ch0224/presentation/pages/forgot_password_page.dart';
import 'package:ch0224/presentation/pages/home_page.dart';
import 'package:ch0224/presentation/pages/sign_up_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../dialogs.dart';
import '../theme/colors.dart';
import '../widgets/custom_text_field.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({super.key});


  @override
  State<LogInPage> createState() => _LogInPageState();
}
bool isChecked = false;
class _LogInPageState extends State<LogInPage> {

  LogInPresenter presenter = LogInPresenter();

  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Welcome Back',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Fill in your email and password to continue',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChange: onChanged,
                  controller: email
              ),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChange: onChanged,
                  controller: password
              ),
              const SizedBox(height: 17),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 21,
                        width: 21,
                        child: Checkbox(
                            value: isChecked,
                            activeColor: colors.primary,
                            side: BorderSide(color: colors.subText, width: 1),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)
                            ),
                            onChanged: (value){
                              setState(() {
                                isChecked = value!;
                              });
                            }
                        ),
                      ),
                      const SizedBox(width: 4),
                      Text(
                        'Remember password',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 12),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPasswordPage()));
                    },
                    child: Text(
                      'Forgot Password?',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent, fontSize: 12),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 187),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        presenter.pressLogIn(
                            email.text,
                            password.text,
                                (_) => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => HomePage()
                                    )
                                ),
                                (error) => showError(context, error),
                        );
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => SignUpPage()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Already have an account?',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign up',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent)
                          )
                        ]
                    )
                ),
              ),
              const SizedBox(height: 18),
              Text(
                'or sign in using',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 8),
              SvgPicture.asset("assets/google.svg")
            ],
          ),
        ),
      ),
    );
  }
}
