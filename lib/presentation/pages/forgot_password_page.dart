import 'package:ch0224/domain/forgot_password_presenter.dart';
import 'package:ch0224/presentation/pages/log_in_page.dart';
import 'package:ch0224/presentation/pages/otp_verification_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../dialogs.dart';
import '../theme/colors.dart';
import '../widgets/custom_text_field.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}
bool isChecked = false;
class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  ForgotPasswordPresenter presenter = ForgotPasswordPresenter();


  TextEditingController email = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Forgot Password',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter your email address',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 56),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChange: onChanged,
                  controller: email
              ),
              const SizedBox(height: 56),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        presenter.pressResetPassword(
                            email.text,
                                (_) => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => OTPVerificationPage(
                                            email: email.text)
                                    )
                                ), (error) => showError(context, error));
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Send OTP',
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogInPage()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Remember password? Back to ',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign in',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent)
                          )
                        ]
                    )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
