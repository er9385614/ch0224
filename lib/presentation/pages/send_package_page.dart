import 'package:ch0224/presentation/pages/send_package_page_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../main.dart';
import '../widgets/custom_field.dart';



class SendPackagePage extends StatefulWidget {
  const SendPackagePage({super.key});

  @override
  State<SendPackagePage> createState() => _SendPackagePageState();

}

class _SendPackagePageState extends State<SendPackagePage> {


  var pointAddress = TextEditingController();
  var pointCountry = TextEditingController();
  var pointPhone = TextEditingController();
  var pointOthers = TextEditingController();
  var destinationAddress = TextEditingController();
  var destinationCountry = TextEditingController();
  var destinationPhone = TextEditingController();
  var destinationOthers = TextEditingController();
  var packageItem = TextEditingController();
  var packageWeight = TextEditingController();
  var packageWorth = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);

    return Scaffold(
        backgroundColor: colors.background,
        resizeToAvoidBottomInset: false,
        body: Column(
          children: [
            SizedBox(
              height: 108,
              width: double.infinity,
              child: Column(
                children: [
                  SizedBox(
                    height: 106,
                    width: double.infinity,
                    child: Column(
                      children: [
                        const SizedBox(height: 73,),
                        Row(
                          children: [
                            const SizedBox(width: 15),
                            GestureDetector(
                              onTap: (){
                                Navigator.of(context).pop();
                              },
                              child: SvgPicture.asset("assets/back.svg"),
                            ),
                            const SizedBox(width: 99),
                            Text(
                              'Send a package',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 2,
                    decoration: const BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(38, 0, 0, 0),
                            blurRadius: 2,
                            offset: Offset(0, 2),
                          )
                        ]
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal:  24),
                child: Column(
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(height: 43),
                          Row(
                            children: [
                              SvgPicture.asset("assets/point.svg"),
                              const SizedBox(width: 8),
                              Text(
                                'Origin Details',
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                              )
                            ],
                          ),
                          CustomTextFieldPage(
                              hint: 'Address',
                              controller: pointAddress
                          ),
                          CustomTextFieldPage(
                              hint: 'State,Country',
                              controller: pointCountry
                          ),
                          CustomTextFieldPage(
                              hint: 'Phone number',
                              controller: pointPhone
                          ),
                          CustomTextFieldPage(
                              hint: 'Others',
                              controller: pointOthers
                          ),
                          const SizedBox(height: 39),
                          Row(
                            children: [
                              SvgPicture.asset('assets/point_2.svg'),
                              const SizedBox(width: 8),
                              Text(
                                'Destination Details',
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                              )
                            ],
                          ),
                          CustomTextFieldPage(
                              hint: 'Address',
                              controller: destinationAddress
                          ),
                          CustomTextFieldPage(
                              hint: 'State,Country',
                              controller: destinationCountry
                          ),
                          CustomTextFieldPage(
                              hint: 'Phone number',
                              controller: destinationPhone
                          ),
                          CustomTextFieldPage(
                              hint: 'Others',
                              controller: destinationOthers
                          ),
                          const SizedBox(height: 11),
                          Row(
                            children: [
                              SvgPicture.asset("assets/add.svg"),
                              const SizedBox(width: 6),
                              Text(
                                'Add destination',
                                style: TextStyle(
                                    color: colors.subText,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 13),
                          Row(
                            children: [
                              Text(
                                'Package Details',
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                              ),
                            ],
                          ),
                          CustomTextFieldPage(
                              hint: 'package items',
                              controller: packageItem
                          ),
                          CustomTextFieldPage(
                              hint: 'Weight of item(kg)',
                              controller: packageWeight
                          ),
                          CustomTextFieldPage(
                              hint: 'Worth of Items',
                              controller: packageWorth
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 39),
                    Row(
                      children: [
                        Text(
                          'Select delivery type',
                          style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 14),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 159,
                          height: 75,
                          decoration: const BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromARGB(38, 0, 0, 0),
                                  blurRadius: 2,
                                  offset: Offset(0, 2),
                                )]
                          ),
                          child: Expanded(
                              child: FilledButton(
                                  style: FilledButton.styleFrom(
                                      backgroundColor: colors.accent,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      )
                                  ),
                                  onPressed: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => SendPackagePage2(
                                      pointAddress: pointAddress.text,
                                      pointCountry: pointCountry.text,
                                      pointPhone: pointPhone.text,
                                      pointOthers: pointOthers.text,
                                      destinationAddress: destinationAddress.text,
                                      destinationCountry: destinationCountry.text,
                                      destinationPhone: destinationPhone.text,
                                      destinationOthers: destinationOthers.text,
                                      packageItem: packageItem.text,
                                      packageWeight: packageWeight.text,
                                      packageWorth: packageWorth.text,
                                    )
                                    )
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 13,),
                                      SvgPicture.asset('assets/clock.svg'),
                                      const SizedBox(height: 10),
                                      const Text(
                                        'Instant delivery',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14
                                        ),
                                      )
                                    ],
                                  )
                              )
                          ),
                        ),
                        const SizedBox(width: 24),
                        Container(
                          width: 159,
                          height: 75,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(8)),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromARGB(38, 0, 0, 0),
                                  blurRadius: 2,
                                  offset: Offset(0, 2),
                                )]
                          ),
                          child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  side: const BorderSide(color: Colors.white),
                                  backgroundColor: colors.background,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                              onPressed: (){},
                              child: Column(
                                children: [
                                  const SizedBox(height: 13,),
                                  SvgPicture.asset('assets/date.svg'),
                                  const SizedBox(height: 10),
                                  Text(
                                    'Scheduled delivery',
                                    style: TextStyle(
                                        color: colors.subText,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12
                                    ),
                                  ),
                                ],
                              )
                          ),
                        )
                      ],
                    )
                  ],
                )
            ),
          ],
        )
    );
  }
}