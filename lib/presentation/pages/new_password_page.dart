import 'package:ch0224/domain/new_password_presenter.dart';
import 'package:ch0224/presentation/pages/log_in_page.dart';
import 'package:flutter/material.dart';

import '../dialogs.dart';
import '../widgets/custom_text_field.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});


  @override
  State<NewPassword> createState() => _NewPasswordState();
}
bool isChecked = false;
class _NewPasswordState extends State<NewPassword> {

  NewPasswordPresenter presenter = NewPasswordPresenter();

  TextEditingController confirmPassword = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    void onChanged(_){}
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 155, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'New Password',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter new password',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 46),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChange: onChanged,
                  controller: password
              ),
              CustomTextField(
                  label: 'Confirm Password',
                  hint: '**********',
                  enableObscure: true,
                  onChange: onChanged,
                  controller: confirmPassword
              ),
              const SizedBox(height: 71),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        presenter.pressResetPassword(
                            password.text,
                                (_) => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => LogInPage()
                        )
                      ), (error) => showError(context, error));
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Log in',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
