import 'dart:io';

import 'package:ch0224/data/models/model_profile.dart';
import 'package:ch0224/domain/profile_presenter.dart';
import 'package:ch0224/main.dart';
import 'package:ch0224/presentation/dialogs.dart';
import 'package:ch0224/presentation/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../widgets/custom_list_title.dart';
import '../add_payment_page.dart';
import '../notification_page.dart';
import '../send_package_page.dart';

class ProfileTab extends StatefulWidget{
  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {

  ProfilePresenter presenter = ProfilePresenter();

  bool isSee = true;
  bool isDark = false;

  var name = "";
  var balance = "";
  @override

  void initState(){
    super.initState();
    presenter.getProfile(
            (ModelProfile currentUser){
          setState(() {
            name = currentUser.fullName;
            balance = currentUser.balance.toString();
          });
        }, (String e) {showError(context, e);}
    );
  }
  @override
  Widget build(BuildContext context) {

    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Column(
        children: [
          SizedBox(
            height: 108,
            width: double.infinity,
            child: Column(
              children: [
                const SizedBox(height: 73),
                Center(
                  child: Text(
                    'Profile',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 16),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 2,
            decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(38, 0, 0, 0),
                    blurRadius: 2,
                    offset: Offset(0, 2),
                  )]
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 17, left: 24, right: 24),
            child: Column(
              children: [
                SizedBox(
                  height: 75,
                  child: ListTile(
                    contentPadding: const EdgeInsets.symmetric(vertical: 10),
                    leading: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: colors.hint
                        ),
                        child: Icon(Icons.person, color: colors.text)
                    ),
                    title: Text(
                        name,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16)
                    ),
                    subtitle: RichText(
                      text: TextSpan(
                          children: [
                            TextSpan(
                                text: "Current balance: ",
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 12)
                            ),
                            TextSpan(
                                text: (isSee) ? "N$balance:00" : "*"*5,
                                style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 12, color: colors.accent)
                            )
                          ]
                      ),
                    ),
                    trailing: GestureDetector(
                        onTap: (){
                          setState(() {
                            isSee = !isSee;
                          });
                        },
                        child: SvgPicture.asset('assets/eye-slash.svg')
                    ),
                  ),
                ),
                const SizedBox(height: 19),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                        'Enable dark Mode',
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: 16)
                    ),
                    Switch(
                        inactiveThumbColor: Colors.white,
                        inactiveTrackColor: const Color.fromARGB(255, 215, 215, 215),
                        trackOutlineColor: MaterialStateProperty.resolveWith((states) => colors.background),
                        value: isDark,
                        onChanged: (value){
                          setState(() {
                            isDark = !isDark;
                            MyApp.of(context).changeDifferentTheme(context);
                          });
                        }
                    )
                  ],
                ),
                CustomListTile(
                    color: colors.text,
                    icon: SvgPicture.asset("assets/profile.svg"),
                    label: "Edit Profile",
                    text: 'Name, phone no, address, email ...'
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => SendPackagePage()
                        )
                    );
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset("assets/statements.svg"),
                      color: colors.text,
                      label: "Statements & Reports",
                      text: 'Download transaction details, orders, deliveries'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => NotificationPage()
                        )
                    );
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset("assets/notification.svg"),
                      color: colors.text,
                      label: "Notification Settings",
                      text: 'mute, unmute, set location & tracking setting'
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => AddPaymentPage()
                        )
                    );
                  },
                  child: CustomListTile(
                      icon: SvgPicture.asset("assets/card.svg"),
                      color: colors.text,
                      label: "Card & Bank account settings",
                      text: 'change cards, delete card details'
                  ),
                ),
                CustomListTile(
                    icon: SvgPicture.asset('assets/referrals.svg'),
                    color: colors.text,
                    label: "Referrals",
                    text: 'check no of friends and earn'
                ),
                CustomListTile(
                    icon: SvgPicture.asset("assets/map.svg"),
                    color: colors.text,
                    label: "About Us",
                    text: 'know more about us, terms and conditions'
                ),
                GestureDetector(
                  onTap: (){
                    showLoading(context);
                    presenter.pressSignOut(
                            (_) => exit(0),
                            (error) => showError(context, error));
                    Navigator.of(context).pop();
                  },
                  child: CustomListTile(
                    icon: SvgPicture.asset("assets/log_out.svg"),
                    color: colors.error,
                    label: "Log Out",
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}