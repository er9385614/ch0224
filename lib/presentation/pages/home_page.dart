import 'package:ch0224/presentation/pages/sign_up_page.dart';
import 'package:ch0224/presentation/pages/tabs/home_tab.dart';
import 'package:ch0224/presentation/pages/tabs/profile_tab.dart';
import 'package:ch0224/presentation/pages/tabs/track_tab.dart';
import 'package:ch0224/presentation/pages/tabs/wallet_tab.dart';
import 'package:ch0224/presentation/theme/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget{

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    var colors = LightColorsApp();
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: colors.accent,
        elevation: 0,
        type: BottomNavigationBarType.fixed,
        iconSize: 14,
        onTap: (newIndex){
          setState(() {
            currentIndex = newIndex;
          });
        },
        showUnselectedLabels: true,
        showSelectedLabels: true,
        selectedFontSize: 14,
        unselectedFontSize: 14,
        unselectedItemColor: colors.subText,
        currentIndex: currentIndex,
        items: [
          BottomNavigationBarItem(
            label: "Home",
              icon: (currentIndex == 0)
                  ? SvgPicture.asset("assets/home_selected.svg")
                  : SvgPicture.asset("assets/home.svg")
          ),
          BottomNavigationBarItem(
              label: "Wallet",
              icon: (currentIndex == 1)
                  ? SvgPicture.asset("assets/wallet_selected.svg")
                  : SvgPicture.asset("assets/wallet.svg")
          ),
          BottomNavigationBarItem(
              label: "Track",
              icon: (currentIndex == 2)
                  ? SvgPicture.asset("assets/track_selected.svg")
                  : SvgPicture.asset("assets/track.svg")
          ),
          BottomNavigationBarItem(
              label: "Profile",
              icon: (currentIndex == 3)
                  ? SvgPicture.asset("assets/profile_selected.svg")
                  : SvgPicture.asset("assets/profile.svg")
          ),
        ],),
      body: [HomeTab(), WalletTab(), TrackTab(), ProfileTab()]
      [currentIndex]
    );
  }
}