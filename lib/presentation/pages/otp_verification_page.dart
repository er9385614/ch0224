import 'package:ch0224/presentation/pages/new_password_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

import '../../domain/otp_verification_presenter.dart';
import '../dialogs.dart';
import '../theme/colors.dart';

class OTPVerificationPage extends StatefulWidget {
  final String email;
  const OTPVerificationPage({super.key, required this.email});


  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}
bool isChecked = false;
class _OTPVerificationPageState extends State<OTPVerificationPage> {

  OTPVerificationPresenter presenter = OTPVerificationPresenter();
  
  TextEditingController code = TextEditingController();
  
  @override
  void initState(){
    super.initState();
    presenter.timer(
            (){
          setState(() {
          });
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 158, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'OTP Verification',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Enter the 6 digit numbers sent to your email',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 52),
              Pinput(
                  length: 6,
                  controller: code,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  defaultPinTheme: PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1, color: colors.subText)
                      ),
                      textStyle: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Colors.black
                      )
                  ),
                  submittedPinTheme: PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: colors.primary)
                    ),
                    textStyle: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.black
                    ),
                  )
              ),
              const SizedBox(height: 20),
              (presenter.getSeconds() != 0) ? Text(
                'If you didn’t receive code, resend after ${presenter.getSeconds().toString()}',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ) : GestureDetector(
                onTap: (){
                  setState(() {
                    presenter.refresh(60);
                  });
                },
                child: RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'If you didn’t receive code, ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                        ),
                        TextSpan(
                            text: 'resend',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: colors.primary)
                        )
                      ]
                  ),
                ),
              ),
              const SizedBox(height: 84),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        showLoading(context);
                        presenter.pressVerifyOTP(
                            widget.email,
                            code.text,
                                (_) => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => const NewPassword()
                                    )
                                ),
                                (error) => showError(context, error));
                        Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: Text(
                        'Set New Password',
                      )
                  )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
