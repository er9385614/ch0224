import 'package:ch0224/domain/sign_up_presenter.dart';
import 'package:ch0224/presentation/pages/home_page.dart';
import 'package:ch0224/presentation/pages/log_in_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../dialogs.dart';
import '../theme/colors.dart';
import '../widgets/custom_text_field.dart';


class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});


  @override
  State<SignUpPage> createState() => _SignUpPageState();
}
bool isChecked = false;
class _SignUpPageState extends State<SignUpPage> {
  SignUpPresenter presenter = SignUpPresenter();

  TextEditingController name = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();

  void onChanged(_){

  }
  @override
  Widget build(BuildContext context) {
    var colors = LightColorsApp();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 78, left: 24, right: 24),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Create an account',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 8),
              Row(
                children: [
                  Text(
                    'Complete the sign up process to get started',
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ],
              ),
              const SizedBox(height: 9),
              CustomTextField(
                  label: 'Full name',
                  hint: 'Ivanov Ivan',
                  onChange: onChanged,
                  controller: name
              ),
              CustomTextField(
                  label: 'Phone Number',
                  hint: '+7(999)999-99-99',
                  onChange: onChanged,
                  controller: phone
              ),
              CustomTextField(
                  label: 'Email Address',
                  hint: '***********@mail.com',
                  onChange: onChanged,
                  controller: email
              ),
              CustomTextField(
                  label: 'Password',
                  hint: '**********',
                  enableObscure: true,
                  onChange: onChanged,
                  controller: password
              ),CustomTextField(
                  label: 'Confirm Password',
                  hint: '**********',
                  enableObscure: true,
                  onChange: onChanged,
                  controller: confirmPassword
              ),
              const SizedBox(height: 31),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 21,
                    width: 21,
                    child: Checkbox(
                        value: isChecked,
                        activeColor: colors.accent,
                        side: BorderSide(
                            color: colors.subText, width: 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2)
                        ),
                        onChanged: (value){
                          setState(() {
                            isChecked = value!;
                          });
                        }
                    ),
                  ),
                  const SizedBox(width: 11),
                  Expanded(
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              children: [
                                TextSpan(
                                    text: 'By ticking this box, you agree to our ',
                                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                        fontSize: 12)
                                ),
                                TextSpan(
                                    text: 'Terms and conditions and private policy',
                                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                        fontSize: 12,
                                        color: colors.primary)
                                )
                              ]
                          )
                      )
                  ),
                ],
              ),
              const SizedBox(height: 64),
              SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                      onPressed: (){
                        // showLoading(context);
                        // presenter.pressSignUp(
                        //     email.text,
                        //     phone.text,
                        //     name.text,
                        //     password.text,
                        //         (_) =>
                                    Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (_) => HomePage()
                        ));
                        //         (error) => showError(context, error));
                        // Navigator.of(context).pop();
                      },
                      style: Theme.of(context).filledButtonTheme.style,
                      child: const Text(
                        'Sign Up',
                      )
                  )
              ),
              const SizedBox(height: 20),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => const LogInPage()));
                },
                child: RichText(
                    text: TextSpan(
                        children: [
                          TextSpan(
                              text: 'Already have an account?',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                          ),
                          TextSpan(
                              text: 'Sign in',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent)
                          )
                        ]
                    )
                ),
              ),
              const SizedBox(height: 18),
              Text(
                'or sign in using',
                style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(height: 8),
              SvgPicture.asset("assets/google.svg",),
              const SizedBox(height: 28,)
            ],
          ),
        ),
      ),
    );
  }
}
