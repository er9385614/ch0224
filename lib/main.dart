import 'package:ch0224/presentation/pages/sign_up_page.dart';
import 'package:ch0224/presentation/theme/colors.dart';
import 'package:ch0224/presentation/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
      url: "https://zlesswvdwrvnumywajpk.supabase.co",
      anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InpsZXNzd3Zkd3J2bnVteXdhanBrIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTMzNjcwODAsImV4cCI6MjAyODk0MzA4MH0.OQjX2pUbWBk31UvPYAmUfbhyM2T8pvzwTz8V5bucuzI"
  );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  State<MyApp> createState() => _MyAppState();

  var isLightTheme = true;

  void changeDifferentTheme(BuildContext context) {
    isLightTheme = !isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()!.onChangeTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? light : dark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? lightTheme : darkTheme;
  }
}

class _MyAppState extends State<MyApp> {
  void onChangeTheme(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: SignUpPage(),
    );
  }
}
