import 'package:ch0224/data/repository/supabase.dart';
import 'package:ch0224/domain/utils.dart';

class NewPasswordPresenter{

  Future<void> pressResetPassword(
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError,
      ) async {
    requestResetPassword() async {
      await changePassword(password);
    }
    request(requestResetPassword, onResponse, onError);
  }
}