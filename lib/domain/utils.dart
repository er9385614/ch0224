import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void request<T>(
    Future<T> Function() request,
    Function(T) onResponse,
    Function(String) onError
  ) async {
  try {
  if (!await checkNetworkConnection()) {
  await onError("Network error");
  } else {
  dynamic response = await request();
  onResponse(response);
  }
  } on AuthException catch (e){
  onError(e.message);
  } on PostgrestException catch (e){
  onError(e.message);
  } on Exception catch (e){
  onError(e.toString());
  }
}


Future<bool> checkNetworkConnection() async {
  var result = await Connectivity().checkConnectivity();
  return result != ConnectivityResult.none;
}


bool checkEmail(String email){
  return RegExp(r"^[0-9a-z]+@[a-z]+\.[a-z]{2,}$").hasMatch(email);
}

bool checkPassword(String password){
  return password.isNotEmpty;
}
