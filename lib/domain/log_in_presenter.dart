import 'package:ch0224/data/models/model_auth.dart';
import 'package:ch0224/data/repository/supabase.dart';
import 'package:ch0224/domain/utils.dart';

class LogInPresenter{

  Future<void> pressLogIn(
      String email,
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestLogIn() async {
      await signIn(ModelAuth(email: email, password: password));
    }
    request(requestLogIn, onResponse, onError);
  }
}