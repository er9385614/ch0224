import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

class SendPackagePresenter{

  Future<void> pressSendPackage(
      String addressOrder,
      String countryOrder,
      String phoneOrder,
      String? othersOrder,
      String items,
      int weight,
      int worth,
      int charges,
      int instant,
      String addressDes,
      String countryDes,
      String phoneDes,
      String othersDes,
      Function onResponse,
      Function(String) onError) async {
    try {
      saveOrders(
          addressOrder,
          countryOrder,
          phoneOrder,
          othersOrder!,
          items,
          weight as String,
          worth as String,
          charges,
          instant,
          addressDes,
          countryDes,
          phoneDes,
          othersDes);
      onResponse();
    } on AuthException catch (e) {
      onError(e.message);
    } on PostgrestException catch (e) {
      onError(e.toString());
    } on Exception catch (e) {
      onError(e.toString());
    }
  }

}