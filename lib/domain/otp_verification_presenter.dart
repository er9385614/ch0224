import 'dart:async';

import 'package:ch0224/data/repository/supabase.dart';
import 'package:ch0224/domain/utils.dart';

import '../data/storage/timer.dart';

class OTPVerificationPresenter{
  void time(){
    if (seconds != 0){
      seconds --;
    }
  }

  void timer(Function callback){
    Timer(
        const Duration(seconds: 1), (){
      time();
      callback();
      timer(callback);
    });
  }

  int getSeconds(){
    return seconds;
  }

  void refresh(int num){
    seconds = num;
  }

  void pressVerifyOTP(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError,
      ){
    requestVerifyOTP() async {
      await verifyOTP(email, code);
    }
    request(requestVerifyOTP, onResponse, onError);
  }
}


