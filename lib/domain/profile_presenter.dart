import 'package:ch0224/data/models/model_profile.dart';
import 'package:ch0224/data/repository/supabase.dart';
import 'package:ch0224/data/storage/profiles.dart';
import 'package:ch0224/domain/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class ProfilePresenter{

  Future<void> getProfile(
      Function onResponse,
      Function(String) onError) async {
    try{
      profiles = await getUserData();
      var currentId = getUserId();
      ModelProfile currentUser = profiles.where((element) => element.idUser == currentId).single;
      onResponse(currentUser);
    }on AuthException catch(e){
      onError(e.message);
    }on PostgrestException catch(e){
      onError(e.toString());
    }on Exception catch(e){
      onError(e.toString());
    }
  }

  void pressSignOut(
      Function(void) onResponse,
      Future<void> Function(String) onError
      ){
    requestSignOut() async {
      await signOut();
    }
    request(requestSignOut, onResponse, onError);
  }
}