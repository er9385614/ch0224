import 'package:ch0224/data/models/model_auth.dart';
import 'package:ch0224/domain/utils.dart';

import '../data/repository/supabase.dart';

class SignUpPresenter{

  Future<void> pressSignUp(
      String email,
      String phone,
      String fullName,
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError)
  async {
    requestSignUp() async {
      var response = await signUp(ModelAuth(email: email, password: password));
      await dataSignUp(response.user!.id, phone, fullName);
    }
    request(requestSignUp, onResponse, onError);
  }

}