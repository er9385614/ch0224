import 'package:ch0224/data/repository/supabase.dart';
import 'package:ch0224/domain/utils.dart';

class ForgotPasswordPresenter{
  Future<void> pressResetPassword(
      String email,
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestResetPassword() async {
      await sendOTP(email);
    }
    request(requestResetPassword, onResponse, onError);
  }
}